<!DOCTYPE html>
<html lang="en" style="height: auto;"><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AdminLTE 3 | Blank Page</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&amp;display=fallback">
        <link rel="stylesheet" href="assets/css/font-awesome/all.min.css">
        <link rel="stylesheet" href="assets/css/adminlte.min.css">
    <body class="sidebar-mini" style="height: auto;">

        <div class="wrapper">

            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <?= $this->include('common/navbar') ?>
            </nav>

            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <?= $this->include('common/sidebar') ?>
            </aside>

            <div class="content-wrapper" style="min-height: 1604.8px;">

                <section class="content-header">
                    <div class="container-fluid">
                        <h1><?= $this->renderSection('page_title') ?></h1>
                    </div>
                </section>
                    
                <section class="content">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Subtitulo</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                           <?= $this->renderSection('content') ?>
                        </div>

                        

                    </div>


                    
                </section>

            </div>

            <footer class="main-footer">
                <?= $this->include('common/footer') ?>
            </footer>

        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/adminlte.min.js"></script>
    </body>
</html>

